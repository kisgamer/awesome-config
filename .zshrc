
PS1="%F{cyan}%~ %F{green}> %F{white}"
echo '\e[5 q'

alias pman='doas pacman'
alias music='tmux new-session -s $$ "tmux source-file ~/.ncmpcpp/tsession"'
alias ls="eza --icons --group-directories-first"
alias km="tmux kill-session -t"


autoload -U compinit; compinit
zstyle ':completion:*' menu select

source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

pfetch

bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char

source /home/floti/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

