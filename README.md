# AwesomeWM, rofi and zsh dotfiles
These are my personal preferences

## Installation
Move everything from the ```.config``` folder in the repo to ```~/.config/```

Move ```.zshrc``` to ```~/```

Refer to https://gitlab.com/kisgamer/xdg-autostart for further instructions.

Done!
